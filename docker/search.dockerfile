FROM python:3.9

RUN mkdir /search_app/
WORKDIR /search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY src src
COPY src/search/search/main.py src/search/main.py

CMD ["python", "src/search/main.py"]
