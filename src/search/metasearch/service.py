from typing import List, Dict

from search.service import BaseSearchService
from user.service import UserService
import requests
import json
import pandas as pd
import sys


class MetaSearchService:
    def __init__(self, search, user_service) -> None:
        self._search = search
        self._user_service = user_service

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        params = {
            "user_id": user_id
        }
        user_data = requests.get(f"{self._user_service}/get_user_data", params=params).text
        # user_data = self._user_service.get_user_data(user_id)  # {'gender': ..., 'age': ...}
        params = {
            "search_text": search_text,
            "user_data": user_data,
            "limit": limit
        }
        df = pd.DataFrame(json.loads(requests.get(f"{self._search}/get_search_data", params=params).text))
        # df = self._search.get_search_data(search_text, user_data, limit)
        doc_columns = json.loads(requests.get(f"{self._search}/get_doc_columns").text)
        return df[doc_columns].to_dict('records')
