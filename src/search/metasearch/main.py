from metasearch.http import Server
from metasearch.service import MetaSearchService

def main():
    user_service = "http://user:8000"
    search = "http://search:8000"
    metasearch = MetaSearchService(search, user_service)
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
