from flask import Flask, request

from user.service import UserService


class Server(Flask):
    def __init__(self, name: str, user_service: UserService):
        super().__init__(name)
        self._user = user_service
        urls = [
            ('/get_user_data', self.get_user_data, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_user_data(self):
        user_id = int(request.args.get('user_id'))
        return self._user.get_user_data(user_id)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
