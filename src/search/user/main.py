from common.data_source import CSV
from user.http import Server
from settings import USER_DATA_FILE
from user.service import UserService


def main():
    user_service = UserService(CSV(USER_DATA_FILE))
    server = Server('user', user_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
