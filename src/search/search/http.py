from flask import Flask, request

from search.service import BaseSearchService

import json

class Server(Flask):
    def __init__(self, name: str, search: BaseSearchService):
        super().__init__(name)
        self._search = search
        urls = [
            ('/get_search_data', self.get_search_data, {}),
            ('/get_doc_columns', self.get_doc_columns, {})
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_search_data(self):
        search_text = request.args.get('search_text')
        user_data = json.loads(request.args.get('user_data'))
        limit = int(request.args.get('limit'))
        sr = self._search.get_search_data(search_text, user_data)
        return json.dumps(sr)

    def get_doc_columns(self):
        return json.dumps(self._search.DOCS_COLUMNS)

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
