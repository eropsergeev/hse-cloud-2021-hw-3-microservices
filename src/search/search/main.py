from common.data_source import CSV
from search.http import Server
from search.service import SearchInShardsService, SimpleSearchService
from settings import SEARCH_DOCUMENTS_DATA_FILE_PREFIX, datafile
from search.service import SimpleSearchService, SearchInShardsService
import os


def main():
    shard = int(os.environ.get("SHARD_NUMBER", 0))
    shards = int(os.environ.get("TOTAL_SHARDS", 1))
    if not shard:
        search = SearchInShardsService(shards = ["http://shard_" + str(i) + ":8000" for i in range(1, shards + 1)])
    else:
        search = SimpleSearchService(CSV(datafile(SEARCH_DOCUMENTS_DATA_FILE_PREFIX + "." + str(shard) + ".csv")))
    server = Server('search', search)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
